remove EMS_grid_Va.txt;

#printf "clear all;\n" >> EMS_grid_Va.txt;

# *********************************************************
# ---------------- FASE A -------------------------
printf "Fase A: Linhas = nós (aprox. 30)// colunas = timesteps (48)" >> EMS_grid_Va.txt;
for {i in Ob} {
	printf "\n" >> EMS_grid_Va.txt;
	for {t in T}{
		printf "%6.4f ",  (sqrt(Va_sqr[i,t])/(Vnom)) >> EMS_grid_Va.txt;
}}
#printf "]; \n" >> EMS_grid_Va.txt;

remove EMS_grid_Vb.txt;

#printf "clear all;\n" >> EMS_grid_Vb.txt;

# *********************************************************
# ---------------- FASE B -------------------------
printf "Fase B: Linhas = nós (aprox. 30)// colunas = timesteps (48)" >> EMS_grid_Vb.txt;
for {i in Ob} {
	printf "\n" >> EMS_grid_Vb.txt;
	for {t in T}{
		printf "%6.4f ",  (sqrt(Vb_sqr[i,t])/(Vnom)) >> EMS_grid_Vb.txt;
}}
#printf "]; \n" >> EMS_grid_Vb.txt;

remove EMS_grid_Vc.txt;

#printf "clear all;\n" >> EMS_grid_Vc.txt;

# *********************************************************
# ---------------- FASE C -------------------------
printf "Fase C: Linhas = nós (aprox. 30)// colunas = timesteps (48)" >> EMS_grid_Vc.txt;
for {i in Ob} {
	printf "\n" >> EMS_grid_Vc.txt;
	for {t in T}{
		printf "%6.4f ",  (sqrt(Vc_sqr[i,t])/(Vnom)) >> EMS_grid_Vc.txt;
}}
#printf "]; \n" >> EMS_grid_Vc.txt;

remove EMS_grid_Ia.txt;

#printf "clear all;\n" >> EMS_grid_Ia.txt;

# *********************************************************
# ---------------- Carregamento Fase A -------------------------
#printf "Carregamento Fase A: Linhas = linhas (aprox. 39)// colunas = timesteps (48)" >> EMS_grid_Ia.txt;
for {(i,j) in Ol} {
	printf "\n" >> EMS_grid_Ia.txt;
	for {t in T}{
		printf "%8.4f ",  ((sqrt((Pa_sqr[i,j,t] + Qa_sqr[i,j,t])/(Va_sqr[j,t]))/Imax[i,j])*100) >> EMS_grid_Ia.txt;
}}
#printf "]; \n" >> EMS_grid_Ia.txt;

remove EMS_grid_Ib.txt;

#printf "clear all;\n" >> EMS_grid_Ib.txt;

# *********************************************************
# ---------------- Carregamento Fase B -------------------------
#printf "Carregamento Fase B: Linhas = linhas (aprox. 39)// colunas = timesteps (48)" >> EMS_grid_Ib.txt;
for {(i,j) in Ol} {
	printf "\n" >> EMS_grid_Ib.txt;
	for {t in T}{
		printf "%8.4f ",  ((sqrt((Pb_sqr[i,j,t] + Qb_sqr[i,j,t])/(Vb_sqr[j,t]))/Imax[i,j])*100) >> EMS_grid_Ib.txt;
}}
#printf "]; \n" >> EMS_grid_Ib.txt;

remove EMS_grid_Ic.txt;

#printf "clear all;\n" >> EMS_grid_Ic.txt;

# *********************************************************
# ---------------- Carregamento Fase C -------------------------
#printf "Carregamento Fase C: Linhas = linhas (aprox. 39)// colunas = timesteps (48)" >> EMS_grid_Ic.txt;
for {(i,j) in Ol} {
	printf "\n" >> EMS_grid_Ic.txt;
	for {t in T}{
		printf "%8.4f ",  ((sqrt((Pc_sqr[i,j,t] + Qc_sqr[i,j,t])/(Vc_sqr[j,t]))/Imax[i,j])*100) >> EMS_grid_Ic.txt;
}}
#printf "]; \n" >> EMS_grid_Ic.txt;


remove EMS_grid_Plss.txt;

#printf "clear all;\n" >> EMS_grid_Plssa.txt;

# *********************************************************
# ---------------- Perdas Fase A -------------------------
#printf "Perdas Fase A: Linhas = linhas (aprox. 39)// colunas = timesteps (48)" >> EMS_grid_Plssa.txt;

#printf "\n" >> EMS_grid_Plss.txt;
for {t in T} {
	printf "%8.4f ",  (sum{(i,j) in Ol} (Plss_a[i,j,t] + Plss_b[i,j,t] + Plss_c[i,j,t])) >> EMS_grid_Plss.txt;
}
