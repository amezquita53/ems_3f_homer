function [simulation_state, matlab_simulation_variables] = MatlabDispatch(simulation_parameters, simulation_state, matlab_simulation_variables)
    %masterpath = pwd;
    %modelpath = char([pwd,'\ems_3f_homer']);
    
    %EMS_dispatch;
    %PS
    %PB 

    time_step = simulation_state.current_timestep + 1;

    % Definition of timestep (to read EMS dispatch)
    daily_timesteps = 48;
    remainder = rem(time_step, daily_timesteps);
    
    %if (time_step == (48*8)-1)
    %    time_step
    %end

    %  PVS(1) = GMU_PV
    %  PVS(2) = + PV 

    if (time_step == 1)
        fileID = fopen('sim_ctrl.txt', 'r');
        formatSpec = '%u';
        simulation_number = fscanf(fileID, formatSpec, [1 Inf]);
        fclose(fileID);

        fileID = fopen('sim_ctrl.txt','a');
        formatSpec = '\n%u';
        fprintf(fileID,formatSpec, simulation_number(end)+1);
        fclose(fileID);

        fileName = sprintf('ems_3f_grid_%i.txt',simulation_number(end)+1);
        % Creates Grid Archive to save the grid status for determining wich dispatch strategy will be used.
        fileID = fopen(fileName,'w');
        formatSpec = '%u %u\n';
        fprintf(fileID,formatSpec, 0, 0);
        fclose(fileID);

        %fileID = fopen('ems_3f_timestep.txt','w');
        %formatSpec = 'Timestep File \n';
        %fprintf(fileID,formatSpec);
        %fclose(fileID);

        %fileID = fopen('ems_3f_input.txt','w');
        %formatSpec = 'Input File \n';
        %fprintf(fileID,formatSpec);
        %fclose(fileID);

        fileName = sprintf('ems_3f_dispatch_%i.txt',simulation_number(end)+1);
        fileID = fopen(fileName,'w');
        formatSpec = 'Timestep, Load, Grid Pur, Grid Sell, PV1 Power, PV2 Power, Bat Power, Bat SoC kWh, Gen Power\n';
        fprintf(fileID,formatSpec);
        fclose(fileID);
    end

    fileID = fopen('sim_ctrl.txt', 'r');
    formatSpec = '%u';
    simulation_number = fscanf(fileID, formatSpec, [1 Inf]);
    fclose(fileID);

    number = simulation_number(end);

    fileName = sprintf('ems_3f_grid_%i.txt',number);
    fileID = fopen(fileName, 'r');
    formatSpec = '%u %u';
    gridstatus = fscanf(fileID, formatSpec, [2 Inf]);
    fclose(fileID);
    
    [g_row, g_col] = size(gridstatus);
    gridmode = gridstatus(2,g_col);
    
    if (gridstatus(1,g_col) ~= simulation_state.grids(1).grid_state.grid_is_down || remainder == 1)

        if (gridstatus(1,g_col) == 0 && simulation_state.grids(1).grid_state.grid_is_down == true)
            %ENTRA NO MODO DE CONTINGÊNCIA
            gridmode = 1;
        elseif (gridstatus(1,g_col) == 1 && simulation_state.grids(1).grid_state.grid_is_down == false)
            %VERIFICAR SE PÓS CONTINGÊNCIA OU EMS
            if(remainder == 1)
                % EMS
                gridmode = 0;
            else
                %MODO PÓS CONTINGÊNCIA
                gridmode = 2;
            end
        elseif (gridmode == 2 && remainder == 1)
            gridmode = 0;
        end

        fileName = sprintf('ems_3f_grid_%i.txt',number);
        fileID = fopen(fileName, 'a');
        formatSpec = '%u %u\n';
        fprintf(fileID,formatSpec, simulation_state.grids(1).grid_state.grid_is_down, gridmode);
        fclose(fileID);      
    end  

    if (gridmode == 0)

        % OPERAÇÃO COM EMS
        % Primeiro timestep do dia - chamada da função homer EMS
        if (remainder == 1) 

            if (time_step == 1)
                fileID = fopen('ems_3f_sizing.txt','a');
                formatSpec = '%u, %5.1f, %5.1f, %5.1f, %5.1f, %5.1f \n';
                fprintf(fileID, formatSpec, number, simulation_parameters.pvs(1).rated_capacity, simulation_parameters.pvs(2).rated_capacity, simulation_parameters.batteries(1).battery_bank_maximum_absolute_soc, simulation_parameters.converters(1).inverter_capacity/2, simulation_state.generators(1).power_available);
                fclose(fileID);
            end
            % DEBUG
            % simulation_parameters.pvs(1).rated_capacity
            % simulation_parameters.pvs(2).rated_capacity
            % simulation_parameters.converters(1).inverter_efficiency/100
            % simulation_state.batteries(1).state_of_charge_kwh
            % simulation_parameters.batteries(1).battery_bank_maximum_absolute_soc * (simulation_parameters.batteries(1).minimum_state_of_charge/100)
            % simulation_parameters.batteries(1).battery_bank_maximum_absolute_soc
            % 0.5*simulation_parameters.converters(1).inverter_capacity
            % simulation_parameters.generators(1).minimum_load
            % simulation_state.generators(1).power_available
            
            day = int16(1 + (time_step / daily_timesteps));

            % The first day of the year is FRIDAY --> the model needs to know week and weekend days because of energy prices
            % Defining the day of the week
            % 1-Monday 2-Tuesday 3-Wednesday 4-Thursday 5-Friday 6-Saturday 7-Sunday
            initial_day = 5;
            week_day = rem (fix (time_step/48) + initial_day, 7);
            week = 0; % 1 for week days and 2 for weekends

            if (week_day >= 1 && week_day <= 5)
                week = 1; 
            else
                week = 2;
            end


            fileID = fopen('ems_3f_input.txt','a');
            formatSpec = '%8.4f %8.4f %8.4f %8.4f %8.4f %8.4f %8.4f %8.4f %8.4f %8.4f %8.4f\n';
            fprintf(fileID,formatSpec, day, week, simulation_parameters.pvs(1).rated_capacity, simulation_parameters.pvs(2).rated_capacity, simulation_parameters.converters(1).inverter_efficiency/100, simulation_state.batteries(1).state_of_charge_kwh, simulation_parameters.batteries(1).battery_bank_maximum_absolute_soc * (simulation_parameters.batteries(1).minimum_state_of_charge/100), simulation_parameters.batteries(1).battery_bank_maximum_absolute_soc, simulation_parameters.converters(1).inverter_capacity, simulation_parameters.generators(1).minimum_load, simulation_state.generators(1).power_available);
            fclose(fileID);


            % function homerEMS(gdsize                          , day, efficiency                                                 , EBinitial                                        , EBminimum,                                                                                                                               EBmaximum,                                                           PBmaximum )
            % homerEMS(simulation_parameters.pvs(1).rated_capacity, day, simulation_parameters.converters(1).inverter_efficiency/100, simulation_state.batteries(1).state_of_charge_kwh, simulation_parameters.batteries(1).battery_bank_maximum_absolute_soc * (simulation_parameters.batteries(1).minimum_state_of_charge/100), simulation_parameters.batteries(1).battery_bank_maximum_absolute_soc, 0.5*simulation_parameters.converters(1).inverter_capacity, simulation_state.generators(1).power_available*simulation_parameters.generators(1).minimum_load/100, simulation_state.generators(1).power_available);
            % day
            % week 
            % simulation_parameters.pvs(1).rated_capacity
            % simulation_parameters.pvs(2).rated_capacity
            % simulation_parameters.converters(1).inverter_efficiency/100
            % simulation_state.batteries(1).state_of_charge_kwh
            % simulation_parameters.batteries(1).battery_bank_maximum_absolute_soc * (simulation_parameters.batteries(1).minimum_state_of_charge/100), simulation_parameters.batteries(1).battery_bank_maximum_absolute_soc
            % 0.5*simulation_parameters.converters(1).inverter_capacity
            % simulation_parameters.generators(1).minimum_load
            % simulation_state.generators(1).power_available

            homerEMScg_ems_3f_tu(day, week, simulation_parameters.pvs(1).rated_capacity, simulation_parameters.pvs(2).rated_capacity, simulation_parameters.converters(1).inverter_efficiency/100, simulation_state.batteries(1).state_of_charge_kwh, simulation_parameters.batteries(1).battery_bank_maximum_absolute_soc * (simulation_parameters.batteries(1).minimum_state_of_charge/100), simulation_parameters.batteries(1).battery_bank_maximum_absolute_soc, simulation_parameters.converters(1).inverter_capacity, simulation_parameters.generators(1).minimum_load, simulation_state.generators(1).power_available);      
            clear EMS_dispatch;
        end
        % Se o resto da divisão do times step por daily_timesteps for zero, indica que é o último time step do dia
        if remainder == 0 % Resto 0 na divisão = último timestep do dia
            remainder = daily_timesteps;
        end


        % Leitura do arquivo contendo os resultados do EMS e, consequentemente, a operação dos componentes
        % clear EMS_dispatch
        
        EMS_dispatch;
        %if (time_step == 2*48)
        %    remainder
        %    time_step
        %    PB
        %    PS
        %end
        %grid_setpoint = PS(remainder);
        battery_setpoint = PB(remainder);
        % pv_setpoint_1 = PFV1(remainder);
        % pv_setpoint_2 = PFV2(remainder);
        pv_setpoint_1 = simulation_state.pvs(1).power_available();
        pv_setpoint_2 = simulation_state.pvs(2).power_available();
        gd_setpoint = PG(remainder);

        % Definição do setpoint de geração PV
        simulation_state.pvs(1).power_setpoint = pv_setpoint_1;
        simulation_state.pvs(2).power_setpoint = pv_setpoint_2;
        simulation_state.generators(1).power_setpoint = gd_setpoint;

        grid_setpoint = simulation_state.ac_bus.load_requested - (-battery_setpoint + pv_setpoint_1 + pv_setpoint_2 + gd_setpoint);
        
        % Definição da operação da bateria      
        % Verifica se há carga ou descarga da bateria
        if (battery_setpoint == 0) % Não há carga nem descarga da bateria
            simulation_state.batteries(1).power_setpoint = 0;

        elseif (battery_setpoint > 0) % Armazenamento de energia | RETIFICADOR
            simulation_state.batteries(1).power_setpoint = battery_setpoint;
            simulation_state.converters(1).rectifier_power_output = simulation_state.batteries(1).power_setpoint;
            simulation_state.converters(1).rectifier_power_input = (simulation_state.converters(1).rectifier_power_output)/(simulation_parameters.converters(1).rectifier_efficiency/100);

        elseif (battery_setpoint < 0) % Descarga de energia armazenada | INVERSOR
            simulation_state.batteries(1).power_setpoint = battery_setpoint;
            simulation_state.converters(1).inverter_power_input = (-1)*simulation_state.batteries(1).power_setpoint;
            simulation_state.converters(1).inverter_power_output = simulation_state.converters(1).inverter_power_input*(simulation_parameters.converters(1).inverter_efficiency/100);
        end

        % Definição da operação da rede
        % Verifica se há compra ou venda de energia da rede elétrica
        if (grid_setpoint >= 0) % Compra de energia da rede
            simulation_state.grids(1).grid_purchases = grid_setpoint;
            simulation_state.grids(1).grid_sales = 0;
        else % Venda de energia para rede
            simulation_state.grids(1).grid_purchases = 0;
            simulation_state.grids(1).grid_sales = (-1)*grid_setpoint;
        end

        % Definição da operação do gerador térmico 

        % Verifica se naquele timestep ainda tem carga sem ser suprida
        power_remaining = simulation_state.ac_bus.load_requested - (grid_setpoint - battery_setpoint + pv_setpoint_1 + pv_setpoint_2 + gd_setpoint);

        % Caso tenha ainda alguma carga a ser suprida, faz a correção do valor de compra/venda de energia da/para rede
        if (power_remaining >= 0)
            if (grid_setpoint >= 0) 
                simulation_state.grids(1).grid_purchases = grid_setpoint + power_remaining;
            else
                simulation_state.grids(1).grid_sales = (-1)*grid_setpoint - power_remaining;
            end
        else % Se a potência remanescente for menor do que zero, há excesso de energia
            if (grid_setpoint >= 0)
                imulation_state.grids(1).grid_purchases = grid_setpoint - power_remaining;
            else
                simulation_state.grids(1).grid_sales = (-1)*grid_setpoint + power_remaining;
            end
            %simulation_state.ac_bus.excess_electricity =  (-1)*power_remaining;
        end

        % Define a carga servida como a requisitada 
        simulation_state.ac_bus.load_served = simulation_state.ac_bus.load_requested;
        simulation_state.primary_loads(1).load_served = simulation_state.ac_bus.load_served;

        % Define as variáveis obrigatórias do HOMER no barramento AC de capacidade de operação requisitada e servida
        simulation_state.ac_bus.operating_capacity_requested = (simulation_state.ac_bus.load_requested) + (simulation_parameters.operating_reserve.timestep_requirement/100) * (simulation_state.ac_bus.load_requested) + (simulation_parameters.operating_reserve.solar_requirement/100) * (simulation_state.pvs(1).power_setpoint);
        simulation_state.ac_bus.operating_capacity_served = simulation_state.generators(1).power_available + simulation_state.batteries(1).max_discharge_power + simulation_state.grids(1).max_grid_purchases + simulation_state.pvs(1).power_available + simulation_state.pvs(2).power_available;
        
        % Define as variáveis obrigatórias do HOMER no barramento DC de capacidade de operação requisitada e servida
        simulation_state.dc_bus.operating_capacity_requested = 0;
        simulation_state.dc_bus.operating_capacity_served = 0;

        % Para o cálculo da déficit de capacidade é utilizada a diferença entre a capacidade servida 
        % e a capacidade requisitada e, por isso, é feita essa coreção 
        if (simulation_state.ac_bus.operating_capacity_served > simulation_state.ac_bus.operating_capacity_requested)
            simulation_state.ac_bus.operating_capacity_served = simulation_state.ac_bus.operating_capacity_requested;
        end

        % Cálculo do déficit de capacidade
        simulation_state.ac_bus.capacity_shortage = simulation_state.ac_bus.operating_capacity_requested - simulation_state.ac_bus.operating_capacity_served;

        % Armazenamento de dados em arquivos para debugar as simulações

        %fileID = fopen('timestep_ems_3f.txt','a');
        %formatSpec = '%8.2f\n';
        %fprintf(fileID,formatSpec, simulation_state.current_timestep );
        %fclose(fileID);
    
    elseif (gridmode == 1) 
        total_pv_available = simulation_state.pvs(1).power_available + simulation_state.pvs(2).power_available;
        power_remaining = total_pv_available - simulation_state.ac_bus.load_requested;
        max_load_gs = simulation_state.generators(1).power_available;
        min_load_gs = simulation_parameters.generators(1).minimum_load;
        max_charge = min(simulation_parameters.converters(1).rectifier_capacity, simulation_state.batteries(1).max_charge_power);
        max_discharge = min(simulation_parameters.converters(1).inverter_capacity, simulation_state.batteries(1).max_discharge_power/(simulation_parameters.converters(1).inverter_efficiency/100));
                
        if (power_remaining >= 0) % PV equal or bigger than the required load

            % Charges the BSS with the remaining PV power or its maximum charge capacity
            if (power_remaining <= max_charge)
                simulation_state.converters(1).rectifier_power_input = power_remaining;
            else
                simulation_state.converters(1).rectifier_power_input = max_charge;
            end

            % Defines HOMER state variables related to BSS charge
            simulation_state.converters(1).rectifier_power_output = (simulation_state.converters(1).rectifier_power_input)*(simulation_parameters.converters(1).rectifier_efficiency/100);
            simulation_state.batteries(1).power_setpoint = simulation_state.converters(1).rectifier_power_output;
            
            total_pv_req = simulation_state.ac_bus.load_requested + simulation_state.converters(1).rectifier_power_input;

            if (total_pv_req > simulation_state.pvs(1).power_available)
                simulation_state.pvs(1).power_setpoint = simulation_state.pvs(1).power_available; 
                simulation_state.pvs(2).power_setpoint = total_pv_req - simulation_state.pvs(1).power_setpoint;
            else
                simulation_state.pvs(1).power_setpoint = total_pv_req; 
                simulation_state.pvs(2).power_setpoint = 0; 
            end
            simulation_state.ac_bus.load_served = simulation_state.ac_bus.load_requested;
            % Supplies the demand && the operating capacity

        else
        % PV power may be available && is, for sure, less than load
            power_remaining = simulation_state.ac_bus.load_requested;

            if (total_pv_available > 0)
            % PV is available  
                simulation_state.pvs(1).power_setpoint = simulation_state.pvs(1).power_available;
                simulation_state.pvs(2).power_setpoint = simulation_state.pvs(2).power_available;

                power_remaining = power_remaining - simulation_state.pvs(1).power_setpoint - simulation_state.pvs(2).power_setpoint ;

                if (max_discharge > 0)
                    if (power_remaining <= max_discharge)
                        % USES PV && BSS to supply the demand
                        simulation_state.converters(1).inverter_power_output = power_remaining;
                        simulation_state.converters(1).inverter_power_input = (simulation_state.converters(1).inverter_power_output)/(simulation_parameters.converters(1).inverter_efficiency/100);
                        simulation_state.batteries(1).power_setpoint = (-1)*simulation_state.converters(1).inverter_power_input;
                        simulation_state.ac_bus.load_served = simulation_state.ac_bus.load_requested;
                    elseif (power_remaining <= max_discharge + min_load_gs)
                        % uses PV, BSS && genset minimum load to supply the demand
                        simulation_state.generators(1).power_setpoint = min_load_gs;
                        power_remaining = power_remaining - min_load_gs;
                        simulation_state.converters(1).inverter_power_output = power_remaining;
                        simulation_state.converters(1).inverter_power_input = (simulation_state.converters(1).inverter_power_output)/(simulation_parameters.converters(1).inverter_efficiency/100);
                        simulation_state.batteries(1).power_setpoint = (-1)*simulation_state.converters(1).inverter_power_input;
                        simulation_state.ac_bus.load_served = simulation_state.ac_bus.load_requested;
                    elseif (power_remaining <= max_discharge + max_load_gs)
                        % uses PV, BSS && genset between minimum && maximum load to supply the demand
                        simulation_state.converters(1).inverter_power_output = max_discharge;
                        simulation_state.converters(1).inverter_power_input = (simulation_state.converters(1).inverter_power_output)/(simulation_parameters.converters(1).inverter_efficiency/100);
                        simulation_state.batteries(1).power_setpoint = (-1)*simulation_state.converters(1).inverter_power_input;
                        power_remaining = power_remaining - max_discharge;
                        simulation_state.generators(1).power_setpoint = power_remaining;
                        simulation_state.ac_bus.load_served = simulation_state.ac_bus.load_requested;                        
                    else
                        % uses PV, BSS && genset maximum load to supply the demand, but there is unmet load
                        simulation_state.generators(1).power_setpoint = max_load_gs;
                        simulation_state.converters(1).inverter_power_output = max_discharge;
                        simulation_state.converters(1).inverter_power_input = (simulation_state.converters(1).inverter_power_output)/(simulation_parameters.converters(1).inverter_efficiency/100);
                        simulation_state.batteries(1).power_setpoint = (-1)*simulation_state.converters(1).inverter_power_input;

                        power_remaining = power_remaining - max_load_gs - max_discharge;
                        simulation_state.ac_bus.load_served = total_pv_available + max_load_gs + max_discharge;
                    end

                    simulation_state.ac_bus.operating_capacity_served = max_discharge;

                else
                % BSS is empty
                    if (power_remaining <= min_load_gs)
                        % Checks if the remaining power, considering PV generation, is lower than genset minimum load
                        if (simulation_state.ac_bus.load_requested >= min_load_gs)
                        % Checks if demand, without pv, is higher than genset minimum load

                            simulation_state.generators(1).power_setpoint = min_load_gs;
                            power_remaining = simulation_state.ac_bus.load_requested - min_load_gs;
                            
                            if (power_remaining >= 0)
                            % Supply the demand with genset + PV
                                if (power_remaining <= total_pv_available)

                                    if (total_pv_available > simulation_state.pvs(1).power_available)
                                        simulation_state.pvs(1).power_setpoint = simulation_state.pvs(1).power_available; 
                                        simulation_state.pvs(2).power_setpoint = total_pv_available - simulation_state.pvs(1).power_setpoint;
                                    else
                                        simulation_state.pvs(1).power_setpoint = total_pv_available; 
                                        simulation_state.pvs(2).power_setpoint = 0; 
                                    end
                                    
                                    simulation_state.ac_bus.load_served = simulation_state.ac_bus.load_requested;
                                elseif (power_remaining <= max_charge)
                                    simulation_state.converters(1).rectifier_power_input = power_remaining;
                                    simulation_state.converters(1).rectifier_power_output = (simulation_state.converters(1).rectifier_power_input)*(simulation_parameters.converters(1).rectifier_efficiency/100);
                                    simulation_state.batteries(1).power_setpoint = simulation_state.converters(1).rectifier_power_output;
                                    simulation_state.ac_bus.load_served = simulation_state.ac_bus.load_requested;                            
                                else
                                    simulation_state.ac_bus.load_served = simulation_state.ac_bus.load_requested; 
                                end
                            
                            else
                                simulation_state.ac_bus.load_served = simulation_state.ac_bus.load_requested - power_remaining;
                            end    
                        end

                    elseif (power_remaining <= max_load_gs)
                        if (total_pv_available > simulation_state.pvs(1).power_available)
                            simulation_state.pvs(1).power_setpoint = simulation_state.pvs(1).power_available; 
                            simulation_state.pvs(2).power_setpoint = total_pv_available - simulation_state.pvs(1).power_setpoint;
                        else
                            simulation_state.pvs(1).power_setpoint = total_pv_available; 
                            simulation_state.pvs(2).power_setpoint = 0; 
                        end
                        simulation_state.generators(1).power_setpoint = power_remaining;
                        simulation_state.ac_bus.load_served = simulation_state.ac_bus.load_requested;
                    else
                        if (total_pv_available > simulation_state.pvs(1).power_available)
                            simulation_state.pvs(1).power_setpoint = simulation_state.pvs(1).power_available; 
                            simulation_state.pvs(2).power_setpoint = total_pv_available - simulation_state.pvs(1).power_setpoint;
                        else
                            simulation_state.pvs(1).power_setpoint = total_pv_available; 
                            simulation_state.pvs(2).power_setpoint = 0; 
                        end
                        simulation_state.generators(1).power_setpoint = max_load_gs;

                        simulation_state.ac_bus.load_served = max_load_gs + total_pv_available;
                    end
                end

            else 
            % SEM PV
                if (max_discharge > 0)
                    if (power_remaining <= max_discharge)
                        % DESCARGA DA BATERIA
                        simulation_state.converters(1).inverter_power_output = power_remaining;
                        simulation_state.converters(1).inverter_power_input = (simulation_state.converters(1).inverter_power_output)/(simulation_parameters.converters(1).inverter_efficiency/100);
                        simulation_state.batteries(1).power_setpoint = (-1)*simulation_state.converters(1).inverter_power_input;
                        simulation_state.ac_bus.load_served = simulation_state.ac_bus.load_requested;
                    elseif (power_remaining <= max_discharge + min_load_gs)

                        simulation_state.generators(1).power_setpoint = min_load_gs;
                        power_remaining = power_remaining - min_load_gs;
                        simulation_state.converters(1).inverter_power_output = power_remaining;
                        simulation_state.converters(1).inverter_power_input = (simulation_state.converters(1).inverter_power_output)/(simulation_parameters.converters(1).inverter_efficiency/100);
                        simulation_state.batteries(1).power_setpoint = (-1)*simulation_state.converters(1).inverter_power_input;
                        simulation_state.ac_bus.load_served = simulation_state.ac_bus.load_requested;
                    elseif (power_remaining <= max_discharge + max_load_gs)

                        simulation_state.converters(1).inverter_power_output = max_discharge;
                        simulation_state.converters(1).inverter_power_input = (simulation_state.converters(1).inverter_power_output)/(simulation_parameters.converters(1).inverter_efficiency/100);
                        simulation_state.batteries(1).power_setpoint = (-1)*simulation_state.converters(1).inverter_power_input;
                        power_remaining = power_remaining - max_discharge;
                        simulation_state.generators(1).power_setpoint = power_remaining;
                        simulation_state.ac_bus.load_served = simulation_state.ac_bus.load_requested;
                    else
                        % Demanda é maior que Gerador e Bateria
                        % Pensar o que fazer nesse caso!
                        simulation_state.converters(1).inverter_power_output = max_discharge;
                        simulation_state.converters(1).inverter_power_input = (simulation_state.converters(1).inverter_power_output)/(simulation_parameters.converters(1).inverter_efficiency/100);
                        simulation_state.batteries(1).power_setpoint = (-1)*simulation_state.converters(1).inverter_power_input;
                        simulation_state.generators(1).power_setpoint = max_load_gs;

                        simulation_state.ac_bus.load_served = max_load_gs + max_discharge;
                    end

                    simulation_state.ac_bus.operating_capacity_served = max_discharge;

                else
                %Não tem carga na bateria
                    if (power_remaining < min_load_gs)
                        simulation_state.generators(1).power_setpoint = min_load_gs;
                        power_remaining = min_load_gs - power_remaining;
                        if (max_charge > 0)
                            if (power_remaining <= max_charge)
                                simulation_state.converters(1).rectifier_power_input = power_remaining;
                            else 
                                simulation_state.converters(1).rectifier_power_input = max_charge;
                            end                   
                            simulation_state.converters(1).rectifier_power_output = (simulation_state.converters(1).rectifier_power_input)*(simulation_parameters.converters(1).rectifier_efficiency/100);
                            simulation_state.batteries(1).power_setpoint = simulation_state.converters(1).rectifier_power_output;                        
                            simulation_state.ac_bus.load_served = simulation_state.ac_bus.load_requested;
                        else
                            simulation_state.ac_bus.load_served = min_load_gs;
                        end

                    elseif (power_remaining >= min_load_gs && power_remaining <= max_load_gs)
                        simulation_state.generators(1).power_setpoint = power_remaining;
                        simulation_state.ac_bus.load_served = simulation_state.ac_bus.load_requested;
                    else 
                        simulation_state.generators(1).power_setpoint = max_load_gs;
                        simulation_state.ac_bus.load_served = max_load_gs;

                    end                    
                end
            end
        end
        if (simulation_state.ac_bus.load_requested >= simulation_state.ac_bus.load_served)
            simulation_state.ac_bus.unmet_load = simulation_state.ac_bus.load_requested - simulation_state.ac_bus.load_served;
            simulation_state.ac_bus.excess_electricity = 0;
        else
            simulation_state.ac_bus.unmet_load = 0;
            simulation_state.ac_bus.excess_electricity = simulation_state.ac_bus.load_served - simulation_state.ac_bus.load_requested;
        end
        simulation_state.primary_loads(1).load_served  = simulation_state.ac_bus.load_served;

        simulation_state.ac_bus.operating_capacity_served = simulation_state.ac_bus.operating_capacity_served + total_pv_available + simulation_state.generators(1).power_available;
        simulation_state.ac_bus.operating_capacity_requested = (simulation_state.primary_loads(1).load_served) + (simulation_parameters.operating_reserve.timestep_requirement/100) * (simulation_state.primary_loads(1).load_served); %additional 10% for load constraint
        
        simulation_state.dc_bus.operating_capacity_served = 0;
        simulation_state.dc_bus.load_served = 0;

        if (simulation_state.ac_bus.operating_capacity_served >= simulation_state.ac_bus.operating_capacity_requested)
            simulation_state.ac_bus.operating_capacity_served = simulation_state.ac_bus.operating_capacity_requested;
            simulation_state.ac_bus.capacity_shortage = 0;
        else
            simulation_state.ac_bus.capacity_shortage = simulation_state.ac_bus.operating_capacity_requested - simulation_state.ac_bus.operating_capacity_served;
        end                

                
        %fileID = fopen('contingency_cg_ems_3f.txt','a');
        %formatSpec = '%8.4f %8.4f %8.4f %8.4f %8.4f %8.4f %8.4f %8.4f %8.4f\n';
        %fprintf(fileID,formatSpec,  simulation_state.ac_bus.load_requested, min_load_gs, max_load_gs, simulation_state.generators(1).power_setpoint, max_charge, max_discharge, simulation_state.batteries(1).power_setpoint, total_pv_available, simulation_state.ac_bus.unmet_load);
        %fclose(fileID);
    elseif (gridmode == 2)
        % MODO PÓS CONTINGÊNCIA!
        total_pv_available = simulation_state.pvs(1).power_available + simulation_state.pvs(2).power_available;
        power_remaining = total_pv_available - simulation_state.ac_bus.load_requested;
        max_charge = min(simulation_parameters.converters(1).rectifier_capacity, simulation_state.batteries(1).max_charge_power);
        max_discharge = min(simulation_parameters.converters(1).inverter_capacity, simulation_state.batteries(1).max_discharge_power/(simulation_parameters.converters(1).inverter_efficiency/100));
                
        if (power_remaining >= 0) % PV equal or bigger than the required load

            % Charges the BSS with the remaining PV power or its maximum charge capacity
            if (power_remaining <= max_charge)
                simulation_state.converters(1).rectifier_power_input = power_remaining;
            else
                simulation_state.converters(1).rectifier_power_input = max_charge;
            end

            % Defines HOMER state variables related to BSS charge
            simulation_state.converters(1).rectifier_power_output = (simulation_state.converters(1).rectifier_power_input)*(simulation_parameters.converters(1).rectifier_efficiency/100);
            simulation_state.batteries(1).power_setpoint = simulation_state.converters(1).rectifier_power_output;

            power_remaining = power_remaining - simulation_state.converters(1).rectifier_power_input;

            simulation_state.grids(1).grid_sales = power_remaining;
            simulation_state.grids(1).grid_purchases = 0;
            
            simulation_state.pvs(1).power_setpoint = simulation_state.pvs(1).power_available; 
            simulation_state.pvs(2).power_setpoint = simulation_state.pvs(2).power_available;
            simulation_state.ac_bus.load_served = simulation_state.ac_bus.load_requested;
            % Supplies the demand && the operating capacity

        else
        % PV power may be available && is, for sure, less than load
            if (total_pv_available > simulation_state.pvs(1).power_available)
                simulation_state.pvs(1).power_setpoint = simulation_state.pvs(1).power_available; 
                simulation_state.pvs(2).power_setpoint = total_pv_available - simulation_state.pvs(1).power_setpoint;
            else
                simulation_state.pvs(1).power_setpoint = total_pv_available; 
                simulation_state.pvs(2).power_setpoint = 0; 
            end
            power_remaining = simulation_state.ac_bus.load_requested - simulation_state.pvs(1).power_setpoint - simulation_state.pvs(2).power_setpoint;

            if (simulation_state.grids(1).power_price >= 0.15)

                if (max_discharge > 0)
                    if (power_remaining <= max_discharge)
                        simulation_state.converters(1).inverter_power_output = power_remaining;
                    else 
                        simulation_state.converters(1).inverter_power_output = max_discharge;
                    end
                    simulation_state.converters(1).inverter_power_input = (simulation_state.converters(1).inverter_power_output)/(simulation_parameters.converters(1).inverter_efficiency/100);
                    simulation_state.batteries(1).power_setpoint = (-1)*simulation_state.converters(1).inverter_power_input;
                    
                    power_remaining = power_remaining - simulation_state.converters(1).inverter_power_input;                
                end
                simulation_state.grids(1).grid_sales = 0;
                simulation_state.grids(1).grid_purchases = power_remaining;  
            else 
                % Not Peak Demand!
                if (max_charge > 0 && simulation_state.grids(1).power_price < 0.1)
                    simulation_state.converters(1).rectifier_power_input = max_charge;             
                    simulation_state.converters(1).rectifier_power_output = (simulation_state.converters(1).rectifier_power_input)*(simulation_parameters.converters(1).rectifier_efficiency/100);
                    simulation_state.batteries(1).power_setpoint = simulation_state.converters(1).rectifier_power_output;
                    power_remaining = power_remaining + simulation_state.converters(1).rectifier_power_input;      
                end
                simulation_state.grids(1).grid_sales = 0;
                simulation_state.grids(1).grid_purchases = power_remaining;
            end
            simulation_state.ac_bus.load_served = simulation_state.ac_bus.load_requested;
        end
        

        if (simulation_state.ac_bus.load_requested >= simulation_state.ac_bus.load_served)
            simulation_state.ac_bus.unmet_load = simulation_state.ac_bus.load_requested - simulation_state.ac_bus.load_served;
            simulation_state.ac_bus.excess_electricity = 0;
        else
            simulation_state.ac_bus.unmet_load = 0;
            simulation_state.ac_bus.excess_electricity = simulation_state.ac_bus.load_served - simulation_state.ac_bus.load_requested;
        end

        simulation_state.primary_loads(1).load_served  = simulation_state.ac_bus.load_served;

        simulation_state.ac_bus.operating_capacity_served = simulation_state.batteries(1).max_discharge_power + simulation_state.ac_bus.operating_capacity_served + total_pv_available + simulation_state.generators(1).power_available + simulation_state.grids(1).max_grid_purchases;
        simulation_state.ac_bus.operating_capacity_requested = (simulation_state.primary_loads(1).load_served) + (simulation_parameters.operating_reserve.timestep_requirement/100) * (simulation_state.primary_loads(1).load_served); %additional 10% for load constraint
        
        simulation_state.dc_bus.operating_capacity_served = 0;
        simulation_state.dc_bus.load_served = 0;

        if (simulation_state.ac_bus.operating_capacity_served >= simulation_state.ac_bus.operating_capacity_requested)
            simulation_state.ac_bus.operating_capacity_served = simulation_state.ac_bus.operating_capacity_requested;
            simulation_state.ac_bus.capacity_shortage = 0;
        else
            simulation_state.ac_bus.capacity_shortage = simulation_state.ac_bus.operating_capacity_requested - simulation_state.ac_bus.operating_capacity_served;
        end
    end 
    %fileID = fopen('ems_3f_timestep.txt','a');
    %formatSpec = '%8.2f\n';
    %fprintf(fileID,formatSpec, simulation_state.current_timestep);
    %fclose(fileID);
    
    % if (time_step < 48)
    fileName = sprintf('ems_3f_dispatch_%i.txt', number);
    fileID = fopen(fileName,'a');
    formatSpec = '%8.4f, %8.4f, %8.4f, %8.4f, %8.4f, %8.4f, %8.4f, %8.4f, %8.4f\n';
    fprintf(fileID,formatSpec, time_step, simulation_state.ac_bus.load_requested, simulation_state.grids(1).grid_purchases, simulation_state.grids(1).grid_sales, simulation_state.pvs(1).power_setpoint, simulation_state.pvs(2).power_setpoint, simulation_state.batteries(1).power_setpoint, simulation_state.batteries(1).state_of_charge_kwh, simulation_state.generators(1).power_setpoint);
    fclose(fileID);
    % end

    %if (time_step == 2*48)
    %    fprintf('lalala');
    %end




end