function homerEMScg_ems_3f_tu(day, week, gdsize1, gdsize2, efficiency, EBinitial, EBminimum, EBmaximum, PBmaximum, PGDminimum, PGDmaximum )
  basef = fileparts(which('homerEMScg_ems_3f_tu'));
  addpath(fullfile(basef));
  modeldirectory = fullfile(basef);
  
  %% Create an AMPL instance
  ampl = AMPL;
  
  ampl.eval('param delta_t := 0.5; param d; param c; param rs_nom_gmu; param rs_nom; ');
  ampl.eval('set Obs:=1..1;');
  ampl.eval('param EBi{Obs}; param Lbs{Obs}; param eta{Obs};  param EBmin{Obs}; param EBmax{Obs}; param PBmax{Obs};');
  ampl.eval('set Ogd:=1..1;')
  ampl.eval('param Lgd{Ogd}; param PG_min{Ogd}; param PG_max{Ogd}; param QG_min{Ogd}; param QG_max{Ogd};')

  d = ampl.getParameter('d');
  d.set(day);

  c = ampl.getParameter('c');
  c.set(week);

%
% PV
%
  PFV1 = ampl.getParameter('rs_nom_gmu');
  PFV1.set(gdsize1/3);

% 
  PFV2 = ampl.getParameter('rs_nom');
  PFV2.set(gdsize2/3);

%
% BESS
% 
  Lbs = ampl.getParameter('Lbs');
  Lbs.set(1, 1171);
%
  eta = ampl.getParameter('eta');
  eta.set(1, efficiency);

% 
  EBi = ampl.getParameter('EBi');
  EBi.set(1, EBinitial);

  EBmin = ampl.getParameter('EBmin');
  EBmin.set(1, EBminimum);

% 
  EBmax = ampl.getParameter('EBmax');
  EBmax.set(1, EBmaximum);

% 
  PBmax = ampl.getParameter('PBmax');
  PBmax.set(1, PBmaximum);

%
% Genset
%
  Lgd = ampl.getParameter('Lgd');
  Lgd.set(1, 1145);
% 
  PGDmin = ampl.getParameter('PG_min');
  PGDmin.set(1, PGDminimum);

% 
  PGDmax = ampl.getParameter('PG_max');
  PGDmax.set(1, PGDmaximum);

%
  QGDmin = ampl.getParameter('QG_min');
  QGDmin.set(1,-0.75 * PGDmaximum);

% 
  QGDmax = ampl.getParameter('QG_max');
  QGDmax.set(1, 0.75 * PGDmaximum);


%  d = ampl.getParameter('d')
%  df = d.getValues();
%  df
%  c = ampl.getParameter('c')
%  df = c.getValues();
%  df
%  PFV1 = ampl.getParameter('rs_nom_gmu')
%  df = PFV1.getValues();
%  df
%  PFV2 = ampl.getParameter('rs_nom')
%  df = PFV2.getValues();
%  df
%  Lbs = ampl.getParameter('Lbs')
%  df = Lbs.getValues();
%  df
%  eta = ampl.getParameter('eta')
%  df = eta.getValues();
%  df
%  EBi = ampl.getParameter('EBi')
%  df = EBi.getValues();
%  df
%  EBmax = ampl.getParameter('EBmax')
%  df = EBmax.getValues();
%  df
%  PBmax = ampl.getParameter('PBmax')
%  df = PBmax.getValues();
%  df
%  Lgd = ampl.getParameter('Lgd')
%  df = Lgd.getValues();
%  df
%  PGDmin = ampl.getParameter('PG_min')
%  df = PGDmin.getValues();
%  df
%  PGDmax = ampl.getParameter('PG_max')
%  df = PGDmax.getValues();
%  df
%  QGDmin = ampl.getParameter('QG_min')
%  df = QGDmin.getValues();
%  df
%  QGDmax = ampl.getParameter('QG_max')
%  df = QGDmax.getValues();
%  df
%  d = ampl.getParameter('delta_t')
%  d = ampl.getSet('Obs')
%  df = d.getValues();
%  df
%  d = ampl.getSet('Ogd')
%  df = d.getValues();
%  df
  % Load from file the ampl model
  ampl.read([modeldirectory  '/EMS_3F_PNL_tu.run']);
  
  ampl.close();
end