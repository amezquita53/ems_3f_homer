#################################### EMS_Fluxo_3F.mod ########################
#
# ------------------- Conjuntos ---------------------
set Ob;						# Conjunto de nós
set Ol within Ob cross Ob;	# Conjunto de ramos
set T:=1..48;						# Conjunto de intervalos de tempo
set Ors; 					# Conjunto de geradores de energia renovável
# set Obs;					# Conjunto de baterry storage
# set Ogd;					# Conjunto de geradores distribuídos despacháveis
set D:=1..365; 				# Conjunto de dias do ano
set C:=1..2;				# Conjunto de custos de energia (1: dias da semana, 2: final de semana)
#
# ------------------- Parâmetros -------------------
param tipo_const symbolic; 	# Determina o tipo de problema: tipo_const = PNL - problema nao linear
param Y:=20;
#
# Nós e cenários
param Tipo{Ob};				# Tipo de barra 0: carga, 1: SE
param PDa{Ob,T,D};			# Potência Ativa de Demanda no nó i, instante t e dia d na fase A
param PDb{Ob,T,D};			# Potência Ativa de Demanda no nó i, instante t e dia d na fase B
param PDc{Ob,T,D};			# Potência Ativa de Demanda no nó i, instante t e dia d na fase C
param PDa_norm{Ob,T};		# Potência Ativa de Demanda normalizada no nó i, instante t na fase A
param PDb_norm{Ob,T};		# Potência Ativa de Demanda normalizada no nó i, instante t na fase B
param PDc_norm{Ob,T};		# Potência Ativa de Demanda normalizada no nó i, instante t na fase C
param PD{T,D};				# Potência Reativa de Demanda nominal
param QDa{Ob,T,D};			# Potência Reativa de Demanda no nó i, instante t e dia d na fase A
param QDb{Ob,T,D};			# Potência Reativa de Demanda no nó i, instante t e dia d na fase B
param QDc{Ob,T,D};			# Potência Reativa de Demanda no nó i, instante t e dia d na fase C
param QDa_norm{Ob,T};		# Potência Reativa de Demanda normalizada no nó i, instante t na fase A
param QDb_norm{Ob,T};		# Potência Reativa de Demanda normalizada no nó i, instante t na fase B
param QDc_norm{Ob,T};		# Potência Reativa de Demanda normalizada no nó i, instante t na fase C
param QD{T,D};				# Potência Reativa de Demanda nominal
param fm{T};				# Fator de demanda
param cost{T,C}; 			# Custo da compra da energia da subestação
#
# Renewable source (Geração Fotovoltaica)
param PRSa{Ors,T,D};		# Potência Nominal Ativa do Gerador renovável na fase A
param PRSb{Ors,T,D};		# Potência Nominal Ativa do Gerador renovável na fase B
param PRSc{Ors,T,D};		# Potência Nominal Ativa do Gerador renovável na fase C
param Lrs{Ors};				# Função que associa o Gerador renovável à um nó do sistema
param frs{Ors};				# Fator multiplicativo para definição da porcentagem da geração renovável adicional da CampusGrid
param PRS_0{T,D};			# Geração renovável fotovoltaica normalizada (para 1kWp)
# param rs_nom:=400/3;			# Potência nominal da geração renovável
# param rs_nom_gmu:=336.8/3;			# Potência nominal da geração renovável GMU
# param d:= 1;
#
# Resistencia y Reactancia Ramos ij fases ABC
param Raa{Ol};				# Resistência no circuito na fase A
param Xaa{Ol};				# Reatância no circuito na fase A
param Rbb{Ol};				# Resistência no circuito na fase B
param Xbb{Ol};				# Reatância no circuito na fase B
param Rcc{Ol};				# Resistência no circuito na fase C
param Xcc{Ol};				# Reatância no circuito na fase C
param Xab{Ol};				# Reatância no circuito na fase AB
param Xbc{Ol};				# Reatância no circuito na fase BC
param Xac{Ol};				# Reatância no circuito na fase AC
param Rab{Ol};				# Reatância no circuito na fase AB
param Rbc{Ol};				# Reatância no circuito na fase BC
param Rac{Ol};				# Reatância no circuito na fase AC
#
# Definicion de la magnitud y angulo de las impedancias de los ramos ij fases ABC
param Zaa{Ol};				# Magnitud de la impedanciano circuito na fase A
param Zbb{Ol};				# Magnitud de la impedanciano circuito na fase B
param Zcc{Ol};				# Magnitud de la impedanciano circuito na fase C
param Zab{Ol};				# Magnitud de la impedancia no circuito na fase AB
param Zbc{Ol};				# Magnitud de la impedancia no circuito na fase BC
param Zac{Ol};				# Magnitud de la impedancia no circuito na fase AC
param Thaa{Ol};				# Angulo de la impedancia no circuito na fase A
param Thbb{Ol};				# Angulo de la impedancia no circuito na fase B
param Thcc{Ol};				# Angulo de la impedancia no circuito na fase C
param Thab{Ol};				# Angulo de la impedancia no circuito na fase AB
param Thbc{Ol};				# Angulo de la impedancia no circuito na fase BC
param Thac{Ol};				# Angulo de la impedancia no circuito na fase AC
#
# Definicion de la resistencia y reactancia de la impedancia equivalente en el modelo lineal
param Raa_p{Ol};			# Resistência no circuito na fase A
param Xaa_p{Ol};			# Reatância no circuito na fase A
param Rbb_p{Ol};			# Resistência no circuito na fase B
param Xbb_p{Ol};			# Reatância no circuito na fase B
param Rcc_p{Ol};			# Resistência no circuito na fase C
param Xcc_p{Ol};			# Reatância no circuito na fase C
param Xab_p{Ol};			# Reatância no circuito na fase AB
param Xbc_p{Ol};			# Reatância no circuito na fase BC
param Xac_p{Ol};			# Reatância no circuito na fase AC
param Rab_p{Ol};			# Reatância no circuito na fase AB
param Rbc_p{Ol};			# Reatância no circuito na fase BC
param Rac_p{Ol};			# Reatância no circuito na fase AC
param Xba_p{Ol};			# Reatância no circuito na fase BA
param Xcb_p{Ol};			# Reatância no circuito na fase CB
param Xca_p{Ol};			# Reatância no circuito na fase CA
param Rba_p{Ol};			# Reatância no circuito na fase BA
param Rcb_p{Ol};			# Reatância no circuito na fase CB
param Rca_p{Ol};			# Reatância no circuito na fase CA
#
# Parametros del EDS
param Vnom;					# Magnitude da tensão nominal em relação ao tempo
param Vmin;					# Magnitude de tensão mínima
param Vmax;					# Magnitude de tensão máxima
#param Sn;					# Potencia 3f base
param Zn;					# Impedancia base del sistema
param Va_0{Ob,T};			# Initial voltage value of phase A
param Vb_0{Ob,T};			# Initial voltage value of phase B
param Vc_0{Ob,T};			# Initial voltage value of phase C
param Tha0{Ob};				# Initial angle of phase A
param Thb0{Ob};				# Initial angle of phase B
param Thc0{Ob};				# Initial angle of pahse C
param Imax{Ol};				# Magnitude máxima de corrente permitido pelo ramo [A]
param Smax{Ob};				# Potência aparente máxima fornecida pela subestação
param Pa_0{Ol,T};			# Approximation of the active power in line i,j of phase A
param Pb_0{Ol,T};			# Approximation of the active power in line i,j of phase B
param Pc_0{Ol,T};			# Approximation of the active power in line i,j of phase C
param Qa_0{Ol,T};			# Approximation of the reactive power in line i,j of phase A
param Qb_0{Ol,T};			# Approximation of the reactive power in line i,j of phase B
param Qc_0{Ol,T};			# Approximation of the reactive power in line i,j of phase C
param Tap{Ol};				# 
#
# Parametros dos GDs (genset) para o modelo não linear
#param PG_min{Ogd};			# potência ativa mínima do GD
#param PG_max{Ogd};			# potência ativa máxima do GD
#param QG_min{Ogd};			# potência reativa mínima do GD
#param QG_max{Ogd};			# potência reativa máxima do GD
#param Lgd{Ogd};				# Função que associa o gerador à um nó do sistema
param alpha_0{Ogd};			# custo operacional constante da GD
param alpha_1{Ogd};			# custo operacional linear da GD
param alpha_2{Ogd};			# custo operacional quadrático da GD
param cost_PG:=35;
param cost_PB:=0.0005125;
#param cost_PB:=0.5125;
#param cost_PB:=100.125;
#param cost_PB:=0;
#param cost_PB:=0.089;
#
# Parametros dos BS (Battery Storage) para o modelo não linear
# param eta{Obs};				# Eficiência do BS
# param delta_t:=0.5;			# Período do intervalo de tempo
# param EBi{Obs}; 			# Energia inicial do BS
# param PBmax{Obs};			# Limite máximo de potência de carregamento e descarregamento do BS
# param EBmin{Obs};			# Nível mínimo de energia no BS
# param EBmax{Obs};			# Nível máximo de energia no BS
# param Lbs{Obs};				# Função que associa o BS à um nó do sistema
param alpha_c:= 0.4;
# ------------------- Variáveis -------------------
# Definicion de las variables para el Não linear
var Pa{Ol,T};				# Fluxo de potência ativa da Fase A
var Pb{Ol,T};				# Fluxo de potência ativa da Fase B
var Pc{Ol,T};				# Fluxo de potência ativa da Fase C
var Qa{Ol,T};				# Fluxo de potência reativa da Fase A
var Qb{Ol,T};				# Fluxo de potência reativa da Fase B
var Qc{Ol,T};				# Fluxo de potência reativa da Fase C
var Va{Ob,T};				# Magnitude de tensão da Fase A
var Vb{Ob,T};				# Magnitude de tensão da Fase B
var Vc{Ob,T};				# Magnitude de tensão da Fase C
var Plss_a{Ol,T};			# Perdas de potência ativa da Fase A
var Plss_b{Ol,T};			# Perdas de potência ativa da Fase B
var Plss_c{Ol,T};			# Perdas de potência ativa da Fase C
var Qlss_a{Ol,T};			# Perdas de potência reativa da Fase A
var Qlss_b{Ol,T};			# Perdas de potência reativa da Fase B
var Qlss_c{Ol,T};			# Perdas de potência reativa da Fase C
var PS_a{Ob,T};				# Potência ativa fornecida pela subestação no nó i na Fase A
var QS_a{Ob,T};				# Potência reativa fornecida pela subestação no nó i na Fase A
var PS_b{Ob,T};				# Potência ativa fornecida pela subestação no nó i na Fase B
var QS_b{Ob,T};				# Potência reativa fornecida pela subestação no nó i na Fase B
var PS_c{Ob,T};				# Potência ativa fornecida pela subestação no nó i na Fase C
var QS_c{Ob,T};				# Potência reativa fornecida pela subestação no nó i na Fase C
var PS{Ob,T};				# Potência ativa total fornecida pela subestação no nó i Fase A, B e C
var QS{Ob,T};				# Potência reativa total fornecida pela subestação no nó i Fase A, B e C
var PS_p_a{Ob,T}>=0;
var PS_p_b{Ob,T}>=0;
var PS_p_c{Ob,T}>=0;
var PS_n_a{Ob,T}>=0;
var PS_n_b{Ob,T}>=0;
var PS_n_c{Ob,T}>=0;
#
# Geradores distribuídos
var PGa{Ogd,T};				# Geração de potência ativa da Fase A
var PGb{Ogd,T};				# Geração de potência ativa da Fase B
var PGc{Ogd,T};				# Geração de potência ativa da Fase C
var QGa{Ogd,T};				# Geração de potência reativa da Fase A
var QGb{Ogd,T};				# Geração de potência reativa da Fase B
var QGc{Ogd,T};				# Geração de potência reativa da Fase C
var PG{Ogd,T};				# Geração de potência ativa total
var QG{Ogd,T};				# Geração de potência reativa total
#
# Baterias
var EB{Obs,T};					# Energia da BS
var PB{Obs,T}; 					# Injeção de potência ativa do BS
var PB_ch{Obs,T}; 				# Potência ativa de carregamento do BS
var PB_dis{Obs,T}; 				# Potência ativa de descarregamento do BS
var PB_ch_a{Obs,T}; 			# Potência ativa de carregamento do BS na Fase A
var PB_ch_b{Obs,T}; 			# Potência ativa de carregamento do BS na Fase B
var PB_ch_c{Obs,T}; 			# Potência ativa de carregamento do BS na Fase C
var PB_dis_a{Obs,T}; 			# Potência ativa de descarregamento do BS na Fase A
var PB_dis_b{Obs,T}; 			# Potência ativa de descarregamento BS na Fase B
var PB_dis_c{Obs,T}; 			# Potência ativa de descarregamento BS na Fase C
#
# ------------------- Modelo Linearizado -------------------
#
# Parametros linearização PS
param PS_ms{Ob,1..Y};
param QS_ms{Ob,1..Y};
param PS_Dsmax{Ob};
param QS_Dsmax{Ob};
#
# Variaveis linearização PS sem contingência
var PSsqr{Ob,T};
var QSsqr{Ob,T};
var PS_Dp{Ob,T,1..Y};
var QS_Dp{Ob,T,1..Y};
var PS_p{Ob,T}>=0;
var QS_p{Ob,T}>=0;
var PS_n{Ob,T}>=0;
var QS_n{Ob,T}>=0;
#
# Parametros linearização dos fluxos de potencia Pa_cc...
param S_ms{Ol,1..Y};
param S_Dsmax{Ol};
#
# Variaveis linearização dos fluxos de potencia Pa_cc.. 
#
var Pa_sqr{Ol,T};
var Qa_sqr{Ol,T};
var Pb_sqr{Ol,T};
var Qb_sqr{Ol,T};
var Pc_sqr{Ol,T};
var Qc_sqr{Ol,T};
var Pa_Dp{Ol,T,1..Y};
var Qa_Dp{Ol,T,1..Y};
var Pb_Dp{Ol,T,1..Y};
var Qb_Dp{Ol,T,1..Y};
var Pc_Dp{Ol,T,1..Y};
var Qc_Dp{Ol,T,1..Y};
var Pa_p{Ol,T}>=0;
var Qa_p{Ol,T}>=0;
var Pb_p{Ol,T}>=0;
var Qb_p{Ol,T}>=0;
var Pc_p{Ol,T}>=0;
var Qc_p{Ol,T}>=0;
var Pa_n{Ol,T}>=0;
var Qa_n{Ol,T}>=0;
var Pb_n{Ol,T}>=0;
var Qb_n{Ol,T}>=0;
var Pc_n{Ol,T}>=0;
var Qc_n{Ol,T}>=0;
#
# Tensões linearizadas
var Va_sqr{Ob,T}>=0;
var Vb_sqr{Ob,T}>=0;
var Vc_sqr{Ob,T}>=0;
#
# Linearizacao da bateria
var b_ch{Obs,T} binary;
var b_dis{Obs,T} binary;

#
############################################################################################################
# 									Função Objetivo
#	Minimiza os custos da compra de energia da rede e a operação do gerador 
############################################################################################################
minimize Custos_Operacao_CS:

	sum{i in Ob, t in T} (cost[t,c] * delta_t * (PS_p_a[i,t] + PS_p_b[i,t] + PS_p_c[i,t]));
	
minimize Custos_Operacao_PL:

	sum{i in Ob, t in T} (cost[t,c] * delta_t * (PS_p_a[i,t] + PS_p_b[i,t] + PS_p_c[i,t]))
	+ sum{n in Ogd, t in T} delta_t * cost_PG * PG[n,t] 
	+ sum{b in Obs, t in T} delta_t * cost_PB * PB_ch[b,t]
	+ 1 * sum{(i,j) in Ol, t in T} (Pa_sqr[i,j,t] + Pb_sqr[i,j,t] + Pc_sqr[i,j,t] + Qa_sqr[i,j,t] + Qb_sqr[i,j,t] + Qc_sqr[i,j,t]);
	
###############################################################
# Parte I: Cold-Start
###############################################################
#-------------------------------------------
# Balance de potencia activa en las lineas CS 
#------------------------------------------- 
subject to active_power_balance_phase_a_CS {i in Ob, t in T : tipo_const == "ColdStart"}:
sum{(j,i) in Ol}(Pa[j,i,t]) - sum{(i,j) in Ol}(Pa[i,j,t]) + PS_a[i,t] = PDa[i,t,d];

subject to active_power_balance_phase_b_CS {i in Ob, t in T : tipo_const == "ColdStart"}:
sum{(j,i) in Ol}(Pb[j,i,t]) - sum{(i,j) in Ol}(Pb[i,j,t]) + PS_b[i,t] = PDb[i,t,d];

subject to active_power_balance_phase_c_CS {i in Ob, t in T : tipo_const == "ColdStart"}:
sum{(j,i) in Ol}(Pc[j,i,t]) - sum{(i,j) in Ol}(Pc[i,j,t]) + PS_c[i,t] = PDc[i,t,d];

#-------------------------------------------
# Balance de potencia reactiva en las lineas CS 
#------------------------------------------- 

subject to reactive_power_balance_phase_a_CS {i in Ob, t in T : tipo_const == "ColdStart"}:
sum{(j,i) in Ol}(Qa[j,i,t]) - sum{(i,j) in Ol}(Qa[i,j,t]) + QS_a[i,t] = QDa[i,t,d];

subject to reactive_power_balance_phase_b_CS {i in Ob, t in T : tipo_const == "ColdStart"}:
sum{(j,i) in Ol}(Qb[j,i,t]) - sum{(i,j) in Ol}(Qb[i,j,t]) + QS_b[i,t] = QDb[i,t,d];

subject to reactive_power_balance_phase_c_CS {i in Ob, t in T : tipo_const == "ColdStart"}:
sum{(j,i) in Ol}(Qc[j,i,t]) - sum{(i,j) in Ol}(Qc[i,j,t]) + QS_c[i,t] = QDc[i,t,d];

#-------------------------------------------
# Definição das potências de compra e venda de energia 
#-------------------------------------------

subject to PS_cost_phase_a_CS {i in Ob, t in T : tipo_const == "ColdStart"}:
PS_a[i,t] = PS_p_a[i,t] - PS_n_a[i,t];

subject to PS_cost_phase_b_CS {i in Ob, t in T : tipo_const == "ColdStart"}:
PS_b[i,t] = PS_p_b[i,t] - PS_n_b[i,t];

subject to PS_cost_phase_c_CS {i in Ob, t in T : tipo_const == "ColdStart"}:
PS_c[i,t] = PS_p_c[i,t] - PS_n_c[i,t];

								
###############################################################
# Parte II: Modelo 3F Linear
###############################################################
#
#-------------------------------------------------------
# Definição das perdas de potência ativa nas linhas 
#-------------------------------------------------------
#
subject to define_plss_line_phase_a_PL {(i,j) in Ol, t in T : tipo_const == "PL"}:
Plss_a[i,j,t] = (1/(Va_0[i,t]*Va_0[i,t]))*(Raa_p[i,j]*Pa_sqr[i,j,t] + Raa_p[i,j]*Qa_sqr[i,j,t]
										 - Xaa_p[i,j]*Pa_0[i,j,t]*Qa[i,j,t] + Xaa_p[i,j]*Qa_0[i,j,t]*Pa[i,j,t]) +
				(1/(Va_0[i,t]*Vb_0[i,t]))*(Rab_p[i,j]*Pb_0[i,j,t]*Pa[i,j,t] + Rab_p[i,j]*Qb_0[i,j,t]*Qa[i,j,t]
										 - Xab_p[i,j]*Pb_0[i,j,t]*Qa[i,j,t] + Xab_p[i,j]*Qb_0[i,j,t]*Pa[i,j,t]) +
				(1/(Va_0[i,t]*Vc_0[i,t]))*(Rac_p[i,j]*Pc_0[i,j,t]*Pa[i,j,t] + Rac_p[i,j]*Qc_0[i,j,t]*Qa[i,j,t]
										 - Xac_p[i,j]*Pc_0[i,j,t]*Qa[i,j,t] + Xac_p[i,j]*Qc_0[i,j,t]*Pa[i,j,t]);

subject to define_plss_line_phase_b_PL {(i,j) in Ol, t in T : tipo_const == "PL"}:
Plss_b[i,j,t] = (1/(Vb_0[i,t]*Va_0[i,t]))*(Rba_p[i,j]*Pa_0[i,j,t]*Pb[i,j,t] + Rba_p[i,j]*Qa_0[i,j,t]*Qb[i,j,t]
										 - Xba_p[i,j]*Pa_0[i,j,t]*Qb[i,j,t] + Xba_p[i,j]*Qa_0[i,j,t]*Pb[i,j,t]) +
				(1/(Vb_0[i,t]*Vb_0[i,t]))*(Rbb_p[i,j]*Pb_sqr[i,j,t] + Rbb_p[i,j]*Qb_sqr[i,j,t]
										 - Xbb_p[i,j]*Pb_0[i,j,t]*Qb[i,j,t] + Xbb_p[i,j]*Qb_0[i,j,t]*Pb[i,j,t]) +
				(1/(Vb_0[i,t]*Vc_0[i,t]))*(Rbc_p[i,j]*Pc_0[i,j,t]*Pb[i,j,t] + Rbc_p[i,j]*Qc_0[i,j,t]*Qb[i,j,t]
										 - Xbc_p[i,j]*Pc_0[i,j,t]*Qb[i,j,t] + Xbc_p[i,j]*Qc_0[i,j,t]*Pb[i,j,t]);

subject to define_plss_line_phase_c_PL {(i,j) in Ol, t in T :  tipo_const == "PL"}:
Plss_c[i,j,t] = (1/(Vc_0[i,t]*Va_0[i,t]))*(Rca_p[i,j]*Pa_0[i,j,t]*Pc[i,j,t] + Rca_p[i,j]*Qa_0[i,j,t]*Qc[i,j,t]
										 - Xca_p[i,j]*Pa_0[i,j,t]*Qc[i,j,t] + Xca_p[i,j]*Qa_0[i,j,t]*Pc[i,j,t]) +
				(1/(Vc_0[i,t]*Vb_0[i,t]))*(Rcb_p[i,j]*Pb_0[i,j,t]*Pc[i,j,t] + Rcb_p[i,j]*Qb_0[i,j,t]*Qc[i,j,t]
										 - Xcb_p[i,j]*Pb_0[i,j,t]*Qc[i,j,t] + Xcb_p[i,j]*Qb_0[i,j,t]*Pc[i,j,t]) +
				(1/(Vc_0[i,t]*Vc_0[i,t]))*(Rcc_p[i,j]*Pc_sqr[i,j,t] + Rcc_p[i,j]*Qc_sqr[i,j,t]
										 - Xcc_p[i,j]*Pc_0[i,j,t]*Qc[i,j,t] + Xcc_p[i,j]*Qc_0[i,j,t]*Pc[i,j,t]);
#
#-----------------------------------------------------
# Definição das perdas de potência reatiVa_0 nas linhas 
#-----------------------------------------------------
#
subject to define_qlss_line_phase_a_PL {(i,j) in Ol, t in T : tipo_const == "PL"}:
Qlss_a[i,j,t] = (1/(Va_0[i,t]*Va_0[i,t]))*(Raa_p[i,j]*Pa_0[i,j,t]*Qa[i,j,t] - Raa_p[i,j]*Qa_0[i,j,t]*Pa[i,j,t]
										 +  Xaa_p[i,j]*Pa_sqr[i,j,t] + Xaa_p[i,j]*Qa_sqr[i,j,t]) +
				(1/(Va_0[i,t]*Vb_0[i,t]))*(Rab_p[i,j]*Pb_0[i,j,t]*Qa[i,j,t] - Rab_p[i,j]*Qb_0[i,j,t]*Pa[i,j,t]
										 +  Xab_p[i,j]*Pb_0[i,j,t]*Pa[i,j,t] + Xab_p[i,j]*Qb_0[i,j,t]*Qa[i,j,t]) +
				(1/(Va_0[i,t]*Vc_0[i,t]))*(Rac_p[i,j]*Pc_0[i,j,t]*Qa[i,j,t] - Rac_p[i,j]*Qc_0[i,j,t]*Pa[i,j,t]
										 +  Xac_p[i,j]*Pc_0[i,j,t]*Pa[i,j,t] + Xac_p[i,j]*Qc_0[i,j,t]*Qa[i,j,t]);

subject to define_qlss_line_phase_b_PL {(i,j) in Ol, t in T : tipo_const == "PL"}:
Qlss_b[i,j,t] = (1/(Vb_0[i,t]*Va_0[i,t]))*(Rba_p[i,j]*Pa_0[i,j,t]*Qb[i,j,t] - Rba_p[i,j]*Qa_0[i,j,t]*Pb[i,j,t]
										 +  Xba_p[i,j]*Pa_0[i,j,t]*Pb[i,j,t] + Xba_p[i,j]*Qa_0[i,j,t]*Qb[i,j,t]) +
				(1/(Vb_0[i,t]*Vb_0[i,t]))*(Rbb_p[i,j]*Pb_0[i,j,t]*Qb[i,j,t] - Rbb_p[i,j]*Qb_0[i,j,t]*Pb[i,j,t]
										 +  Xbb_p[i,j]*Pb_sqr[i,j,t] + Xbb_p[i,j]*Qb_sqr[i,j,t]) +
				(1/(Vb_0[i,t]*Vc_0[i,t]))*(Rbc_p[i,j]*Pc_0[i,j,t]*Qb[i,j,t] - Rbc_p[i,j]*Qc_0[i,j,t]*Pb[i,j,t]
										 +  Xbc_p[i,j]*Pc_0[i,j,t]*Pb[i,j,t] + Xbc_p[i,j]*Qc_0[i,j,t]*Qb[i,j,t]);

subject to define_qlss_line_phase_c_PL {(i,j) in Ol, t in T : tipo_const == "PL"}:
Qlss_c[i,j,t] = (1/(Vc_0[i,t]*Va_0[i,t]))*(Rca_p[i,j]*Pa_0[i,j,t]*Qc[i,j,t] - Rca_p[i,j]*Qa_0[i,j,t]*Pc[i,j,t]
										 +  Xca_p[i,j]*Pa_0[i,j,t]*Pc[i,j,t] + Xca_p[i,j]*Qa_0[i,j,t]*Qc[i,j,t]) +
				(1/(Vc_0[i,t]*Vb_0[i,t]))*(Rcb_p[i,j]*Pb_0[i,j,t]*Qc[i,j,t] - Rcb_p[i,j]*Qb_0[i,j,t]*Pc[i,j,t]
										 +  Xcb_p[i,j]*Pb_0[i,j,t]*Pc[i,j,t] + Xcb_p[i,j]*Qb_0[i,j,t]*Qc[i,j,t]) +
				(1/(Vc_0[i,t]*Vc_0[i,t]))*(Rcc_p[i,j]*Pc_0[i,j,t]*Qc[i,j,t] - Rcc_p[i,j]*Qc_0[i,j,t]*Pc[i,j,t]
										 +  Xcc_p[i,j]*Pc_sqr[i,j,t] + Xcc_p[i,j]*Qc_sqr[i,j,t]);
										 

#
#-------------------------------------------
# Balanço de potencia ativa nas linhas
#-------------------------------------------
#
subject to active_power_balance_phase_a_PL {i in Ob, t in T : tipo_const == "PL"}:
sum{(j,i) in Ol}(Pa[j,i,t]) - sum{(i,j) in Ol}(Pa[i,j,t] + Plss_a[i,j,t]) + PS_a[i,t]
			+ sum{n in Ogd : i = Lgd[n]} (PGa[n,t]) + sum{n in Obs : i = Lbs[n]} (PB_dis_a[n,t]) =
						PDa[i,t,d] - sum{n in Ors : i = Lrs[n] and n > 1} (PRSa[n,t,d] * rs_nom) 
						- sum{n in Ors : i = Lrs[n] and n = 1} (PRSa[n,t,d] * rs_nom_gmu) + sum{n in Obs : i = Lbs[n]} (PB_ch_a[n,t]);

subject to active_power_balance_phase_b_PL {i in Ob, t in T : tipo_const == "PL"}:
sum{(j,i) in Ol}(Pb[j,i,t]) - sum{(i,j) in Ol}(Pb[i,j,t] + Plss_b[i,j,t]) + PS_b[i,t]
			+ sum{n in Ogd : i = Lgd[n]} (PGb[n,t]) + sum{n in Obs : i = Lbs[n]} (PB_dis_b[n,t]) =
						PDb[i,t,d] - sum{n in Ors : i = Lrs[n] and n > 1} (PRSb[n,t,d] * rs_nom) 
						- sum{n in Ors : i = Lrs[n] and n = 1} (PRSa[n,t,d] * rs_nom_gmu) + sum{n in Obs : i = Lbs[n]} (PB_ch_b[n,t]);

subject to active_power_balance_phase_c_PL {i in Ob, t in T : tipo_const == "PL"}:
sum{(j,i) in Ol}(Pc[j,i,t]) - sum{(i,j) in Ol}(Pc[i,j,t] + Plss_c[i,j,t]) + PS_c[i,t]
			+ sum{n in Ogd : i = Lgd[n]} (PGc[n,t]) + sum{n in Obs : i = Lbs[n]} (PB_dis_c[n,t]) =
						PDc[i,t,d] - sum{n in Ors : i = Lrs[n] and n > 1} (PRSc[n,t,d] * rs_nom) 
						- sum{n in Ors : i = Lrs[n] and n = 1} (PRSa[n,t,d] * rs_nom_gmu) + sum{n in Obs : i = Lbs[n]} (PB_ch_c[n,t]);
#
#--------------------------------------------
# Balanço de potencia reativa nas linhas 
#--------------------------------------------
#
subject to reactive_power_balance_phase_a_PL {i in Ob, t in T : tipo_const == "PL"}:
sum{(j,i) in Ol}(Qa[j,i,t]) - sum{(i,j) in Ol}(Qa[i,j,t] + Qlss_a[i,j,t]) + QS_a[i,t]
								+ sum{n in Ogd : i = Lgd[n]} (QGa[n,t]) = QDa[i,t,d];

subject to reactive_power_balance_phase_b_PL {i in Ob, t in T  : tipo_const == "PL"}:
sum{(j,i) in Ol}(Qb[j,i,t]) - sum{(i,j) in Ol}(Qb[i,j,t] + Qlss_b[i,j,t]) + QS_b[i,t]
								+ sum{n in Ogd : i = Lgd[n]} (QGb[n,t]) =  QDb[i,t,d];

subject to reactive_power_balance_phase_c_PL {i in Ob, t in T  : tipo_const == "PL"}:
sum{(j,i) in Ol}(Qc[j,i,t]) - sum{(i,j) in Ol}(Qc[i,j,t] + Qlss_c[i,j,t]) + QS_c[i,t]
								+ sum{n in Ogd : i = Lgd[n]} (QGc[n,t]) = QDc[i,t,d];
#-------------------------------------------
# Definição das potências de compra e venda de energia 
#-------------------------------------------
								
subject to PS_cost_phase_a_PL {i in Ob, t in T : tipo_const == "PL"}:
PS_a[i,t] = PS_p_a[i,t] - PS_n_a[i,t];

subject to PS_cost_phase_b_PL {i in Ob, t in T : tipo_const == "PL"}:
PS_b[i,t] = PS_p_b[i,t] - PS_n_b[i,t];

subject to PS_cost_phase_c_PL {i in Ob, t in T : tipo_const == "PL"}:
PS_c[i,t] = PS_p_c[i,t] - PS_n_c[i,t];

#--------------------------------------------
# Queda de tensão nas linhas 
#--------------------------------------------
#
subject to voltage_drop_phase_a_PL {(i,j) in Ol, t in T : tipo_const == "PL"}:
Va_sqr[i,t] - (Va_sqr[j,t]*(Tap[i,j]^2)) = 2*(Raa_p[i,j] * Pa[i,j,t] + Xaa_p[i,j] * Qa[i,j,t]) + 2*(Rab_p[i,j] * Pb[i,j,t] + Xab_p[i,j] * Qb[i,j,t])
											+ 2*(Rac_p[i,j] * Pc[i,j,t] + Xac_p[i,j] * Qc[i,j,t])
											- (1/(Va_0[i,t]^2))*(Raa_p[i,j]^2 + Xaa_p[i,j]^2)*(Pa_sqr[i,j,t] + Qa_sqr[i,j,t]);
							

subject to voltage_drop_phase_b_PL {(i,j) in Ol, t in T : tipo_const == "PL"}:
Vb_sqr[i,t] - (Vb_sqr[j,t]*(Tap[i,j]^2)) = 2*(Rab_p[i,j] * Pa[i,j,t] + Xab_p[i,j] * Qa[i,j,t]) + 2*(Rbb_p[i,j] * Pb[i,j,t] + Xbb_p[i,j] * Qb[i,j,t])
											+ 2*(Rbc_p[i,j] * Pc[i,j,t] + Xbc_p[i,j] * Qc[i,j,t])
											- (1/(Vb_0[i,t]^2))*(Rbb_p[i,j]^2 + Xbb_p[i,j]^2)*(Pb_sqr[i,j,t] + Qb_sqr[i,j,t]);

subject to voltage_drop_phase_c_PL {(i,j) in Ol, t in T : tipo_const == "PL"}:
Vc_sqr[i,t] - (Vc_sqr[j,t]*(Tap[i,j]^2)) = 2*(Rac_p[i,j] * Pa[i,j,t] + Xac_p[i,j] * Qa[i,j,t]) + 2*(Rbc_p[i,j] * Pb[i,j,t] + Xbc_p[i,j] * Qb[i,j,t])
											+ 2*(Rcc_p[i,j] * Pc[i,j,t] + Xcc_p[i,j] * Qc[i,j,t])
											- (1/(Vc_0[i,t]^2))*(Rcc_p[i,j]^2 + Xcc_p[i,j]^2)*(Pc_sqr[i,j,t] + Qc_sqr[i,j,t]);
		
#
#--------------------------------------------
# Limites de magnitude de tensão
#--------------------------------------------
#
subject to limite_magnitude_tensao_phase_a_PL {(i,j) in Ol, t in T : tipo_const == "PL"}:
Vmin^2 <= Va_sqr[i,t] <= Vmax^2;

subject to limite_magnitude_tensao_phase_b_PL {(i,j) in Ol, t in T : tipo_const == "PL"}:
Vmin^2 <= Vb_sqr[i,t] <= Vmax^2;

subject to limite_magnitude_tensao_phase_c_PL {(i,j) in Ol, t in T : tipo_const == "PL"}:
Vmin^2 <= Vc_sqr[i,t] <= Vmax^2;

#--------------------------------------------
# Limite de potência aparente fornecida pela subestação
#--------------------------------------------
#
subject to limite_potencia_ativa_subestacao_PL {i in Ob, t in T : tipo_const == "PL"}:
PS[i,t] = PS_a[i,t] + PS_b[i,t] + PS_c[i,t];

subject to limite_potencia_reativa_subestacao_PL {i in Ob, t in T : tipo_const == "PL"}:
QS[i,t] = QS_a[i,t] + QS_b[i,t] + QS_c[i,t];
#
#--------------------------------------------
# Modelo para os geradores distribuídos
#--------------------------------------------
#
subject to total_active_power_GD_PNL {i in Ogd, t in T : tipo_const == "PL"}:
PG[i,t] = PGa[i,t] + PGb[i,t] + PGc[i,t];

subject to total_reactive_power_GD_PNL {i in Ogd, t in T : tipo_const == "PL"}:
QG[i,t] = QGa[i,t] + QGb[i,t] + QGc[i,t];

#limite de potência ativa dos GDs -> limite inferior
subject to limite_pot_ativa_inf_PNL {i in Ogd, t in T : tipo_const == "PL"}:
PG_min[i] <= PG[i,t];

#limite de potência ativa dos GDs -> limite superior
subject to limite_pot_ativa_sup_PNL {i in Ogd, t in T : tipo_const == "PL"}:
PG[i,t] <= PG_max[i];

#limite de potência reativa dos GDs -> limite inferior
subject to limite_pot_reativa_inf_PNL {i in Ogd, t in T : tipo_const == "PL"}:
QG_min[i] <= QG[i,t];

#limite de potência reativa dos GDs -> limite superior
subject to limite_pot_reativa_sup_PNL {i in Ogd, t in T : tipo_const == "PL"}:
QG[i,t] <= QG_max[i];

#funcionamento gerador
subject to operacao_conectada_ativa_fase_a {i in Ogd, t in T : tipo_const == "PL"}:
PGa[i,t] = 0;

subject to operacao_conectada_ativa_fase_b {i in Ogd, t in T : tipo_const == "PL"}:
PGb[i,t] = 0;

subject to operacao_conectada_ativa_fase_c {i in Ogd, t in T : tipo_const == "PL"}:
PGc[i,t] = 0;

subject to operacao_conectada_reativa_fase_a {i in Ogd, t in T : tipo_const == "PL"}:
QGa[i,t] = 0;

subject to operacao_conectada_reativa_fase_b {i in Ogd, t in T : tipo_const == "PL"}:
QGb[i,t] = 0;

subject to operacao_conectada_reativa_fase_c {i in Ogd, t in T : tipo_const == "PL"}:
QGc[i,t] = 0;

#
#--------------------------------------------
# Modelo para Baterry Storage (BS)
#--------------------------------------------
#
subject to state_of_charge_1_PL {i in Obs : tipo_const == "PL"}:
EB[i,1] = EBi[i] + PB[i,1] * delta_t;

subject to state_of_charge_2_PL {i in Obs, t in T : (tipo_const == "PL") and t > 1}:
EB[i,t] = EB[i,t-1] + PB[i,t] * delta_t;

subject to total_input_output_power_PL {i in Obs, t in T : tipo_const == "PL"}:
PB[i,t] = - PB_dis[i,t] * 1/(eta[i]) + PB_ch[i,t] * eta[i];

subject to maximum_discharge_rate {i in Obs, t in T : t > 1}:
- alpha_c*EB[i,t] <= PB[i,t];

subject to maximum_charge_rate {i in Obs, t in T : t > 1}:
PB[i,t] <= alpha_c*EB[i,t];

subject to total_charging_PL {i in Obs, t in T : tipo_const == "PL"}:
PB_ch[i,t] = PB_ch_a[i,t] + PB_ch_b[i,t] + PB_ch_c[i,t];

subject to balance_power_charging_PL_1 {i in Obs, t in T : tipo_const == "PL"}:
PB_ch_a[i,t] = PB_ch_b[i,t];

subject to balance_power_charging_PL_2 {i in Obs, t in T : tipo_const == "PL"}:
PB_ch_b[i,t] = PB_ch_c[i,t];

subject to balance_power_charging_PL_3 {i in Obs, t in T : tipo_const == "PL"}:
PB_ch_a[i,t] = PB_ch_c[i,t];

subject to total_discharging_PL {i in Obs, t in T : tipo_const == "PL"}:
PB_dis[i,t] = PB_dis_a[i,t] + PB_dis_b[i,t] + PB_dis_c[i,t];

subject to balance_power_discharging_PL_1 {i in Obs, t in T : tipo_const == "PL"}:
PB_dis_a[i,t] = PB_dis_b[i,t];

subject to balance_power_discharging_PL_2 {i in Obs, t in T : tipo_const == "PL"}:
PB_dis_b[i,t] = PB_dis_c[i,t];

subject to balance_power_discharging_PL_3 {i in Obs, t in T : tipo_const == "PL"}:
PB_dis_a[i,t] = PB_dis_c[i,t];

subject to limits_energy_storage_PL {i in Obs, t in T : tipo_const == "PL"}:
EBmin[i] <= EB[i,t] <= EBmax[i];

#---------------------------------------------------
# Linealizacion de las potencias quadraticas PS e QS - subestação - (SEM CONTINGÊNCIAS)
#---------------------------------------------------

subject to limite_potencia_subestacao_PL {i in Ob, t in T : tipo_const == "PL"}:
PSsqr[i,t] + QSsqr[i,t] <= Smax[i]^2;

subject to define_PSsqr_PL {i in Ob, t in T : tipo_const == "PL"}:
PSsqr[i,t] = sum{y in 1..Y}(PS_ms[i,y]*PS_Dp[i,t,y]);

subject to define_QSsqr_PL {i in Ob, t in T : tipo_const == "PL"}:
QSsqr[i,t] = sum{y in 1..Y}(QS_ms[i,y]*QS_Dp[i,t,y]);

subject to definition_PS_PL {i in Ob, t in T : tipo_const == "PL"}:
PS_p[i,t] - PS_n[i,t] = PS[i,t];

subject to definition_QS_PL {i in Ob, t in T : tipo_const == "PL"}:
QS_p[i,t] - QS_n[i,t] = QS[i,t];

subject to definition_PS_abs_PL {i in Ob, t in T : tipo_const == "PL"}:
PS_p[i,t] + PS_n[i,t] = sum{y in 1..Y}(PS_Dp[i,t,y]);

subject to definition_QS_abs_PL {i in Ob, t in T : tipo_const == "PL"}:
QS_p[i,t] + QS_n[i,t] = sum{y in 1..Y}(QS_Dp[i,t,y]);

subject to definition_block_PS_PL {i in Ob, t in T, y in 1..Y : tipo_const == "PL"}:
0 <= PS_Dp[i,t,y] <= PS_Dsmax[i];

subject to definition_block_QS_PL {i in Ob, t in T, y in 1..Y : tipo_const == "PL"}:
0 <= QS_Dp[i,t,y] <= QS_Dsmax[i];

#	
#---------------------------------------------------
# Linealizacion de las fluxos de potencias quadraticas Pa, Pb, Pc e Qa, Qb e Qc  (SEM CONTINGÊNCIAS)
#---------------------------------------------------
#
subject to limite_maximo_corrente_phase_a_PL {(i,j) in Ol, t in T : tipo_const == "PL"}:
Pa_sqr[i,j,t] + Qa_sqr[i,j,t] <= Imax[i,j]^2 * Va_sqr[j,t];

subject to limite_maximo_corrente_phase_b_PL {(i,j) in Ol, t in T : tipo_const == "PL"}:
Pb_sqr[i,j,t] + Qb_sqr[i,j,t] <= Imax[i,j]^2 * Vb_sqr[j,t];

subject to limite_maximo_corrente_phase_c_PL {(i,j) in Ol, t in T : tipo_const == "PL"}:
Pc_sqr[i,j,t] + Qc_sqr[i,j,t] <= Imax[i,j]^2 * Vc_sqr[j,t];

subject to define_Pa_sqr_PL {(i,j) in Ol, t in T : tipo_const == "PL"}:
Pa_sqr[i,j,t] = sum{y in 1..Y}(S_ms[i,j,y]*Pa_Dp[i,j,t,y]);

subject to define_Qa_sqr_PL {(i,j) in Ol, t in T : tipo_const == "PL"}:
Qa_sqr[i,j,t] = sum{y in 1..Y}(S_ms[i,j,y]*Qa_Dp[i,j,t,y]);

subject to define_Pb_sqr_PL {(i,j) in Ol, t in T : tipo_const == "PL"}:
Pb_sqr[i,j,t] = sum{y in 1..Y}(S_ms[i,j,y]*Pb_Dp[i,j,t,y]);

subject to define_Qb_sqr_PL {(i,j) in Ol, t in T : tipo_const == "PL"}:
Qb_sqr[i,j,t] = sum{y in 1..Y}(S_ms[i,j,y]*Qb_Dp[i,j,t,y]);

subject to define_Pc_sqr_PL {(i,j) in Ol, t in T : tipo_const == "PL"}:
Pc_sqr[i,j,t] = sum{y in 1..Y}(S_ms[i,j,y]*Pc_Dp[i,j,t,y]);

subject to define_Qc_sqr_PL {(i,j) in Ol, t in T : tipo_const == "PL"}:
Qc_sqr[i,j,t] = sum{y in 1..Y}(S_ms[i,j,y]*Qc_Dp[i,j,t,y]);

subject to definition_Pa_PL {(i,j) in Ol, t in T : tipo_const == "PL"}:
Pa_p[i,j,t] - Pa_n[i,j,t] = Pa[i,j,t];

subject to definition_Qa_PL {(i,j) in Ol, t in T : tipo_const == "PL"}:
Qa_p[i,j,t] - Qa_n[i,j,t] = Qa[i,j,t];

subject to definition_Pb_PL {(i,j) in Ol, t in T : tipo_const == "PL"}:
Pb_p[i,j,t] - Pb_n[i,j,t] = Pb[i,j,t];

subject to definition_Qb_PL {(i,j) in Ol, t in T : tipo_const == "PL"}:
Qb_p[i,j,t] - Qb_n[i,j,t] = Qb[i,j,t];

subject to definition_Pc_PL {(i,j) in Ol, t in T : tipo_const == "PL"}:
Pc_p[i,j,t] - Pc_n[i,j,t] = Pc[i,j,t];

subject to definition_Qc_PL {(i,j) in Ol, t in T : tipo_const == "PL"}:
Qc_p[i,j,t] - Qc_n[i,j,t] = Qc[i,j,t];

subject to definition_Pa_abs_PL {(i,j) in Ol, t in T : tipo_const == "PL"}:
Pa_p[i,j,t] + Pa_n[i,j,t] = sum{y in 1..Y}(Pa_Dp[i,j,t,y]);

subject to definition_Qa_abs_PL {(i,j) in Ol, t in T : tipo_const == "PL"}:
Qa_p[i,j,t] + Qa_n[i,j,t] = sum{y in 1..Y}(Qa_Dp[i,j,t,y]);

subject to definition_Pb_bbs_PL {(i,j) in Ol, t in T : tipo_const == "PL"}:
Pb_p[i,j,t] + Pb_n[i,j,t] = sum{y in 1..Y}(Pb_Dp[i,j,t,y]);

subject to definition_Qb_bbs_PL {(i,j) in Ol, t in T : tipo_const == "PL"}:
Qb_p[i,j,t] + Qb_n[i,j,t] = sum{y in 1..Y}(Qb_Dp[i,j,t,y]);

subject to definition_Pcs_PL {(i,j) in Ol, t in T : tipo_const == "PL"}:
Pc_p[i,j,t] + Pc_n[i,j,t] = sum{y in 1..Y}(Pc_Dp[i,j,t,y]);

subject to definition_Qcs_PL {(i,j) in Ol, t in T : tipo_const == "PL"}:
Qc_p[i,j,t] + Qc_n[i,j,t] = sum{y in 1..Y}(Qc_Dp[i,j,t,y]);

subject to definition_block_Pa_PL {(i,j) in Ol, t in T, y in 1..Y : tipo_const == "PL"}:
0 <= Pa_Dp[i,j,t,y] <= S_Dsmax[i,j];

subject to definition_block_Qa_PL {(i,j) in Ol, t in T, y in 1..Y : tipo_const == "PL"}:
0 <= Qa_Dp[i,j,t,y] <= S_Dsmax[i,j];

subject to definition_block_Pb_PL {(i,j) in Ol, t in T, y in 1..Y : tipo_const == "PL"}:
0 <= Pb_Dp[i,j,t,y] <= S_Dsmax[i,j];

subject to definition_block_Qb_PL {(i,j) in Ol, t in T, y in 1..Y : tipo_const == "PL"}:
0 <= Qb_Dp[i,j,t,y] <= S_Dsmax[i,j];

subject to definition_block_Pc_PL {(i,j) in Ol, t in T, y in 1..Y : tipo_const == "PL"}:
0 <= Pc_Dp[i,j,t,y] <= S_Dsmax[i,j];

subject to definition_block_Qc_PL {(i,j) in Ol, t in T, y in 1..Y : tipo_const == "PL"}:
0 <= Qc_Dp[i,j,t,y] <= S_Dsmax[i,j];		
												
#
#--------------------------------------------
# Operação Linear da bateria
#--------------------------------------------
#
subject to operacao_linear_bateria {i in Obs, t in T : tipo_const == "PL"}:
b_ch[i,t] + b_dis[i,t] <=  1;

subject to limits_charging_active_power_PL_1 {i in Obs, t in T : tipo_const == "PL"}:
0 <= PB_ch[i,t];

subject to limits_charging_active_power_PL_2 {i in Obs, t in T : tipo_const == "PL"}:
PB_ch[i,t] <= PBmax[i] * b_ch[i,t];

subject to limits_discharging_active_power_PL_1 {i in Obs, t in T : tipo_const == "PL"}:
0 <= PB_dis[i,t];

subject to limits_discharging_active_power_PL_2 {i in Obs, t in T : tipo_const == "PL"}:
PB_dis[i,t] <= PBmax[i] * b_dis[i,t];