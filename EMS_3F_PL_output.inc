remove EMS_dispatch.m;

#printf "clear all;\n" >> EMS_dispatch.m;

# *********************************************************
# Potência ativa na subestação
printf "PS = [" >> EMS_dispatch.m;
for {t in T} {
	printf "%10.6f ",PS[1,t] >> EMS_dispatch.m;
}
printf "]; \n" >> EMS_dispatch.m;

# Potência da bateria
printf "PB = [" >> EMS_dispatch.m;
for {t in T} {
	printf "%10.6f ",PB[1,t] >> EMS_dispatch.m;
}
printf "]; \n" >> EMS_dispatch.m;

# Potência ativa do GD 
printf "PG = [" >> EMS_dispatch.m;
for {t in T} {
	printf "%10.6f ",PG[1,t] >> EMS_dispatch.m;
}
printf "]; \n" >> EMS_dispatch.m;

# Potência ativa do sistema fotovoltaico 
printf "PFV1 = [" >> EMS_dispatch.m;
for {t in T} {
	printf "%10.6f ", ((PRSa[1,t,d] + PRSb[1,t,d] + PRSc[1,t,d]) * rs_nom_gmu) >> EMS_dispatch.m;
}
printf "]; \n" >> EMS_dispatch.m;

# Potência ativa do sistema fotovoltaico 
printf "PFV2 = [" >> EMS_dispatch.m;
for {t in T} {
	printf "%10.6f ", sum{n in Ors : n > 1}((PRSa[n,t,d] + PRSb[n,t,d] + PRSc[n,t,d]) * rs_nom) >> EMS_dispatch.m;
}
printf "]; \n" >> EMS_dispatch.m;

# Energia da bateria
printf "EB = [" >> EMS_dispatch.m;
for {t in T} {
	printf "%10.6f ",EB[1,t] >> EMS_dispatch.m;
}
printf "]; \n" >> EMS_dispatch.m;


	